require("dotenv").config();
// require("./db");
const express = require("express");
const ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const multer = require("multer");
const cors = require("cors");
const { database } = require("./firebase");
const { addDoc, collection } = require("firebase/firestore");
const port = process.env.PORT || 8000;

const app = express();

// const upload = multer({ dest: 'raw-videos/' }).single('video')
const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./raw-videos");
  },
  filename: function (req, file, callback) {
    const date = new Date().getTime()
    callback(null, `${date}.mp4`);
  },
})

var upload = multer({ storage: storage }).single('video');

app.use("/video-app/converted-videos", express.static("./converted-videos"));
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(express.json());

app.get("/video-app", async (req, res) => {
  try {
    res.send({ messgae: "success API Working" });
  } catch (error) {
    res.send({ error });
  }
});

app.post("/video-app", upload, async (req, res) => {
  const name = new Date().getTime();

  // Reads the raw-videos dir after creation
  fs.readdir("./raw-videos/", (err, files) => {
    if (err) {
      console.log(err)
      return res
      .status(500)
      .send({ message: "Internal Sevrer error", error5: err });
    } else {
      // converts the raw video to add text and push it to converted-videos dir
      const command = ffmpeg({ source: `./raw-videos/${files[files.length-1]}` });
      command
      .format("mp4")
      .videoFilters({
        filter: "drawtext",
        options: {
          text: "What Is Longest River In The World?",
          fontsize: 31,
          fontcolor: "white",
          box: 1,
          boxborderw: 10,
          fontfile: "./adventpro-bold.ttf",
            boxcolor: "black@0.4",
            x: 10,
            y: 10,
          },
        })
        .outputOptions([
          "-frag_duration 100",
          "-strict -2",
          "-movflags frag_keyframe+empty_moov",
          "-pix_fmt yuv420p",
        ])
        .output(`converted-videos/${name}.mp4`)
        .on("error", (err, stdout, stderr) => {
          console.log(stderr)
          res.send({ stderr });
        }).on('end',()=>{
          fs.readdir("./converted-videos", async (err, files) => {
            if (err) {
              console.log(err)
              return res
                .status(500)
                .send({ message: "Internal Sevrer error", error: err });
            } else {
             await addDoc(collection(database, "videos"), {
                media: `http://142.93.219.133/video-app/converted-videos/${
                  files[files.length - 1]
                }`,
                name   ,
              })
                res.send({message:'Video Uploaded Successfully'});
            }
          });
        })
        .run();
    }
  });
});

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
